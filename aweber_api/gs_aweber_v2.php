<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: Oct/20/18
 * Time: 06:11
 * Modified by: Matias Reynolds <reymaro@spoanet.com>
 * Updated at: 06/14/2024
 */

require_once(dirname(__FILE__).'/aweber_api.php');

class GsAweber
{
    private $consumerKey    = '***'; # put your credentials here
    private $consumerSecret = '***'; # put your credentials here
    private $accessToken    = '***'; # put your credentials here
    private $tokenSecret    = '***'; # put your credentials here
    private $list_name      = '***'; # put the List ID here
    private $configFile;

    public function __construct($configFile,$list_name) {
        $this->configFile = $configFile;
        $opciones = parse_ini_file($configFile,false);
        $this->setOption('consumerKey',$opciones);
        $this->setOption('consumerSecret',$opciones);
        $this->setOption('accessToken',$opciones);
        $this->setOption('tokenSecret',$opciones);
        $this->list_name = $list_name;

        //print_log(sprintf("opciones:\n%s",print_r($opciones,true)), 'aweber2.txt');
    }

    private function setOption($varname,&$opcs) {
        if(!isset($opcs[$varname]))
            throw new Exception("parametro {$varname} no definido en archivo de configuracion {$this->configFile}");
        $this->$varname = $opcs[$varname];
    }

    public function agregarSuscriptor($email,$nombre,$ip,$tag=null) {

        $aweber = new AWeberAPI($this->consumerKey, $this->consumerSecret);
        $account = $aweber->getAccount($this->accessToken, $this->tokenSecret);
        print_log(sprintf("buscar lista: [%s]",$this->list_name), 'aweber-integration.log');
        $lists = $account->lists->find(array('name' => $this->list_name));

        //verificar si la lista se encontro
        if(isset($lists[0])) {
            $list = $lists[0];
            print_log(sprintf("lista recuperada:\n%s", print_r($list, true)), 'aweber-integration.log');
        }
        else
            throw new Exception(404,"lista {$this->list_name} no hallada");

        # create a subscriber
        $params = array(
            'email' => $email,
            'ip_address' => $ip,
            'name' => $nombre,
            //'ad_tracking' => 'client_lib_example',
        );
        if(!empty($tag))
            $params['tags'] = array($tag);

        $subscribers = $list->subscribers;
        $new_subscriber = $subscribers->create($params);

        # success!
        //print "A new subscriber was added to the $list->name list!";
        return true;

    }
}
