<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: Oct/20/18
 * Time: 06:11
 */

require_once(dirname(__FILE__).'/aweber_api.php');

class GsAweber
{

    private $consumerKey    = '***'; # put your credentials here
    private $consumerSecret = '***'; # put your credentials here
    private $accessToken    = '***'; # put your credentials here
    private $tokenSecret    = '***'; # put your credentials here
    private $list_name      = '***'; # put the List ID here
    private $configFile;

    public function __construct($configFile) {
        $this->configFile = $configFile;
        $opciones = parse_ini_file($configFile,false);
        //var_dump($opciones);die();
        $this->setOption('consumerKey',$opciones);
        $this->setOption('consumerSecret',$opciones);
        $this->setOption('accessToken',$opciones);
        $this->setOption('tokenSecret',$opciones);
        //$this->setOption('account_id',$opciones);
        $this->setOption('list_name',$opciones);

        $this->logear(sprintf("opciones:\n%s",print_r($opciones,true)));
    }

    private function setOption($varname,&$opcs) {
        if(!isset($opcs[$varname]))
            throw new Exception("parametro {$varname} no definido en archivo de configuracion {$this->configFile}");
        $this->$varname = $opcs[$varname];
    }

    public function agregarSuscriptor($email,$nombre,$tag=null) {

        $aweber = new AWeberAPI($this->consumerKey, $this->consumerSecret);
        $account = $aweber->getAccount($this->accessToken, $this->tokenSecret);
        $this->logear(sprintf("buscar lista: [%s]",$this->list_name));
        $lists = $account->lists->find(array('name' => $this->list_name));

        //verificar si la lista se encontró
        if(isset($lists[0])) {
            $list = $lists[0];
            $this->logear(sprintf("lista recuperada:\n%s", print_r($list, true)));
        }
        else
            throw new Exception(404,"lista {$this->list_name} no hallada");

        # create a subscriber
        $params = array(
            'email' => $email,
            'ip_address' => '127.0.0.1',
            //'ad_tracking' => 'client_lib_example',
            'name' => $nombre,
        );
        if(!empty($tag))
            $params['tags'] = array($tag);

        $subscribers = $list->subscribers;
        $new_subscriber = $subscribers->create($params);

        # success!
        //print "A new subscriber was added to the $list->name list!";
        return true;

    }

    private function logear($contenido) {
        $log_delimitador = "\n-------------------------- ".date('Y-m-d H:i:s')." -----------------------------\n";
        $bytes = file_put_contents('aweberapi-log.txt', $log_delimitador.$contenido, FILE_APPEND);
        if($bytes<=0) {
//            die('no se pudo escribir arhcivo');
        }
    }

}
