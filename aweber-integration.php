<?php
/**
 * Plugin Name: AWeber Integration
 * Description: POST a la URL provista para actualizar suscriptores entre listas.
 * Version: 1.0
 * Author: Matias Reynolds
 * Author URI: mailto:reymaro@spoanet.com
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once(plugin_dir_path(__FILE__) . 'aweber_api/gs_aweber_v2.php');

define('AWEBER_CONFIG', plugin_dir_path(__FILE__) . 'aweber-general.ini');

function aweber_integration_endpoint() {
    $AWeberList = isset($_GET['list_id']) ? sanitize_text_field($_GET['list_id']) : null;
    $SubscriberMail = isset($_GET['mail']) ? sanitize_email($_GET['mail']) : null;
    $SubscriberName = isset($_GET['name']) ? sanitize_text_field($_GET['name']) : null;
    $message = '200 :-)';

    if (is_null($AWeberList) || is_null($SubscriberMail)) {
        $message = "Error: Lista de AWeber o E-mail no declarado.";
        print_log(print_r($message, true), 'aweber-integration.log', true);
        return $message;
    }

    if (!is_null($AWeberList) && !is_null($SubscriberMail)) {
        $aweber = new GsAweber(realpath(AWEBER_CONFIG), $AWeberList);

        try {
            $subscriber = $aweber->agregarSuscriptor($SubscriberMail, $SubscriberName, $_SERVER["REMOTE_ADDR"]);
            $message = sprintf("Suscriptor %s agregado a la lista %s: %s", $SubscriberMail, $AWeberList, $subscriber ? 'SI' : 'NO');
            print_log($message, 'aweber-integration.log');
        } catch (Exception $e) {
            $message = sprintf("Error, codigo: %s | Mensaje, %s - Lista %s.", $e->getCode(), $e->getMessage(), $AWeberList);
            print_log($message, 'aweber-integration.log');
        }
    }

    return $message;
}

add_action('rest_api_init', function () {
    register_rest_route('aweber-integration/v1', '/manage', array(
        'methods' => 'GET',
        'callback' => 'aweber_integration_endpoint',
        'permission_callback' => '__return_true',
    ));
});

function aweber_integration_admin_menu() {
    add_menu_page(
        'AWeber Integration',
        'AWeber Integration',
        'manage_options',
        'aweber-integration',
        'aweber_integration_admin_page',
        'dashicons-email-alt',
        100
    );
}

function aweber_integration_admin_page() {
    ?>
    <div class="wrap">
        <h1>AWeber Integration</h1>
        <p>Al tener activo este plugin, usted puede hacer un POST a la siguiente URL con las variables correspondientes:</p>
        <pre><code><?php echo esc_url(rest_url('aweber-integration/v1/manage')); ?></code></pre>
        <h2>Variables:</h2>
        <ul>
            <li><code>list_id</code> (requerido): ID de la lista de AWeber.</li>
            <li><code>mail</code> (requerido): Email del suscriptor.</li>
            <li><code>name</code> (opcional): Nombre del suscriptor.</li>
        </ul>
    </div>
    <?php
}

add_action('admin_menu', 'aweber_integration_admin_menu');

/* Helpers */
function print_log($content, $file, $delimiter = NULL) {
    $delimiter_line = ($delimiter) ? $delimiter : "\n-------------------------- ".date('Y-m-d H:i:s')." -----------------------------\n";
    $bytes = file_put_contents(plugin_dir_path(__FILE__) . $file, $delimiter_line.$content, FILE_APPEND);
    
    if ($bytes <= 0) {
        die('No se pudo escribir arhcivo.');
    }
}